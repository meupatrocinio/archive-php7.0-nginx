FROM ambientum/php:7.0-nginx

USER root

RUN apt-get update -y && \
    apt-get install -y git && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

CMD ["/home/php-user/start.sh"]